import { MongoClient, MongoClientOptions } from "mongodb";

//NOTE : Following Config is inline for basic usage. However we can use vault as per our requirement.

export interface DBConfigMongo {
    dbName: string;
    serverName: string;
    port: number;
    dbOptions: MongoClientOptions
}

interface DBConfigSQL {
    //Create Config When Required
}

export class DBConfig {

    public static dbconf = {
        "default": {
            dbName: 'local',
            serverName: 'localhost',
            port: 27017,
            dbOptions: {
                bufferMaxEntries: 0,
                // reconnectInterval: 10000, //Uncomment when unifiedTopology set to false
                // reconnectTries: 120, //Uncomment when unifiedTopology set to false
                useNewUrlParser: true,
                useUnifiedTopology : true
            }
        } as DBConfigMongo
        // "db2": {} as DBConfigMongo

    }

    //Assuming vault stored in Mongodb
    public static async GetConfigFromVault() {
        try {

            let vaultconn = await MongoClient.connect(`mongodb://localhost:27017`);
            let vaultdb = vaultconn.db('vault');
            let vaultcollection = vaultdb.collection('dbconf');
            let result = await vaultcollection.find({}).toArray();
            if (result && result.length) this.dbconf = result[0];

        } catch (error) {
            console.log(error);
            console.log('error in Getting Conf From Vault');
        }
    }

}