"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CorsConfig = void 0;
var CorsConfig = /** @class */ (function () {
    function CorsConfig() {
    }
    CorsConfig.confs = {
        "default": {
            optionsSuccessStatus: 200,
            methods: ["GET", "POST", "HEAD", "OPTION", "PATCH", "DELETE"],
            origin: ["localhost:4200"]
        },
        "testroute": {}
    };
    return CorsConfig;
}());
exports.CorsConfig = CorsConfig;
//# sourceMappingURL=cors.js.map