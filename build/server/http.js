"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HTTPServer = void 0;
var express_1 = __importDefault(require("express"));
//Global Router Imports
var Middleware = __importStar(require("../controllers/global/middleware"));
var AssetRouter = __importStar(require("../controllers/global/assets"));
var DefaultRouter = __importStar(require("../controllers/global/default"));
//API Router Imports
var TestRouter = __importStar(require("../controllers/api/test"));
var HTTPServer = /** @class */ (function () {
    function HTTPServer(conf) {
        this.app = express_1.default();
    }
    HTTPServer.INIT = function (conf) {
        if (!HTTPServer.server) {
            HTTPServer.conf = conf;
            HTTPServer.server = new HTTPServer(conf);
            HTTPServer.RegisterRouter();
            HTTPServer.StartServer(conf.PORT);
            return HTTPServer.server;
        }
        else
            return HTTPServer.server;
    };
    HTTPServer.RegisterRouter = function () {
        //Allow Cors For All
        // if (HTTPServer.conf.AllowCors) this.server.app.use(cors(CorsConfig.confs.default));
        // parse application/x-www-form-urlencoded
        this.server.app.use(express_1.default.urlencoded({ extended: false }));
        // parse application/json
        this.server.app.use(express_1.default.json());
        //Middleware route must be stayed at the beginning.
        this.server.app.use(Middleware.router);
        this.server.app.use('/assets', AssetRouter.router);
        //Register API routes Here
        this.server.app.use('/api', TestRouter.router);
        //Default Route Must be added at end.
        this.server.app.use('/', DefaultRouter.router);
    };
    HTTPServer.StartServer = function (port) {
        this.server.app.listen(port, function () { console.log("Server Started on Port : " + port); });
    };
    return HTTPServer;
}());
exports.HTTPServer = HTTPServer;
//# sourceMappingURL=http.js.map