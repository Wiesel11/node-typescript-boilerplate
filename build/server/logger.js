"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Logger = void 0;
var winston = __importStar(require("winston"));
var logger_1 = require("../configs/logger");
// LoggerConf
var Logger = /** @class */ (function () {
    function Logger() {
    }
    Logger.Log = function (msg, level) {
        if (level === void 0) { level = 'info'; }
        try {
            if (this.consoleLogger)
                this.consoleLogger.log(level, msg);
            if (this.fileLogger)
                this.fileLogger.log(level, msg);
        }
        catch (error) {
            console.log(error);
            console.log('error in Logger Debug');
        }
    };
    Logger.Console = function (msg, level) {
        if (level === void 0) { level = 'info'; }
        try {
            if (this.consoleLogger)
                this.consoleLogger.log(level, msg);
        }
        catch (error) {
            console.log(error);
            console.log('error in Logger Console');
        }
    };
    Logger.File = function (msg, level) {
        if (level === void 0) { level = 'info'; }
        try {
            if (this.fileLogger)
                this.fileLogger.log(level, msg);
        }
        catch (error) {
            console.log(error);
            console.log('error in File Logger');
        }
    };
    Logger.CreateLogger = function (colors) {
        try {
            (colors) ? winston.addColors(colors) : undefined;
            this.consoleLogger = winston.loggers.add('console', { levels: logger_1.LoggerConf.levels, transports: logger_1.LoggerConf.consoleTransport });
            this.fileLogger = winston.loggers.add('file', { levels: logger_1.LoggerConf.levels, transports: logger_1.LoggerConf.fileTransport });
        }
        catch (error) {
            console.log(error);
            console.log('error in Creating Logger');
        }
    };
    return Logger;
}());
exports.Logger = Logger;
//# sourceMappingURL=logger.js.map