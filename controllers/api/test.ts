import express from "express";
import { DefaultModel } from "../../models/defaultmodel";
import { HTTPServer } from "../../server/http";
import { Utils } from "../../utils/utils";

const routes = express.Router();


routes.use('/123', async (req, res) => {

    let doc = await DefaultModel.InsertTestDoc();
    console.log('Test 123 Called ');
    res.send(doc);
});

routes.use('/stop', async (req, res) => {
    HTTPServer.StopServer();
    res.send({ msg: 'Stopping' });
});


//Always Use Default Routes at the End to ensure precedence
routes.use('/', async (req, res) => {
    console.log('Test Default Called ');
    await Utils.Sleep(10000);
    res.send('Test Called'); 
    console.log('Response Sent');
});

export const router = routes;